"use strict";

function nextId() {
    if(typeof nextId.counter === 'undefined') {
        nextId.counter = 0;
    }
    nextId.counter += 1;
    return nextId.counter;
}

class ProgressBar {
    constructor(min, max) {
        this.min = min? min : 0;
        this.max = max? max : 1;
        this.progress = 0;
        this.canvas = document.createElement("canvas");
        this.canvas.style.height = "16px";
        this.canvas.style.minWidth = "100px";
        this.context = this.canvas.getContext("2d");
    }

    get ratio() {
        return (this.progress - this.min) / (this.max - this.min);
    }

    render() {
        this.canvas.width = this.canvas.clientWidth;
        this.canvas.height = this.canvas.clientHeight;
        this.context.fillStyle ="#99FF99";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "#11FF11";
        this.context.fillRect(0, 0, this.canvas.width*this.ratio, this.canvas.height);
        this.context.font = Math.floor(this.canvas.height*0.5) + "px monospace";
        this.context.textAlign = "center";
        this.context.textBaseline = "middle";
        this.context.fillStyle = "#000000";
        this.context.fillText("" + this.progress, this.canvas.width/2, this.canvas.height/2);
    }


}
class UpdateQueue {
    constructor() {
        this.timers = [];
    }

    interval(func, interval) {
        this.timers.push(setInterval(func, interval));
    }

    clear() {
        for(let i of this.timers) {
            clearInterval(i);
        }
        this.timers = [];
    }
}

var globalUpdateQueue = new UpdateQueue();

function ajax_get(url) {
    let xml = new XMLHttpRequest();
    xml.onload()
    xml.open("GET", url);
}
class WidgetDbClass {
    constructor() {
        this.widgets = {};
    }
    
    addWidget(name, constructFunction) {
        this.widgets[name] = constructFunction;
    }

    newWidget(name, dataset) {
        if(!this.widgets[name]) {
            console.error("no widget named " + name);
        }
        let widget = this.widgets[name](dataset);
        // so we can get widget from the div
        widget.div.widget = widget;
        return widget;
    }
}

var WidgetDb = new WidgetDbClass();

class DataSource {
    constructor() {
        this.observers = [];
        this.tags = [];
    }

    addObserver(tag, observer) {
        for(let obs of this.observers) {
            if(obs == observer) {
                console.error("observer already added");
                return;
            }
        }
        this.observers.push(obsorver);
        this.tags.push(tag);
    }

    notify() {
        for(let i = 0; i < this.observers.length; i += 1) {
            this.observers[i].dataChanged(tag, this.data);
        }
    }


    set data(value) {
        this._data = value;
        this.notify;
    }

    get data() {
        return this._data;
    }
}



class TableWidget {
    constructor(dataset) {
        this._div = document.createElement("table")

        this.data = dataset.data;
    }

    get div() {
        return this._div;
    }

    update(completion) {
        let html = new StringBuilder();
        html.write("<table><tr><td>Name</td><td>Value</td></tr>");
        for(let key in this.data) {
            if(this.data.hasOwnProperty(key)) {
                html.openTag("tr");
                html.openTag("td");
                html.write(key);
                html.closeTag("td");
                html.openTag("td");
                html.write(this.data[key]);
                html.closeTag("td");
                html.closeTag("tr");
            }
        }
        html.closeTag("table");
        this.div.innerHTML = html.build();
        if(completion) {
            completion();
        }
    }
}

class LoadAvgWidget {
    constructor(dataset) {
        this.updateInterval = 1000;
        this.div = document.createElement("table");
        this.div.innerHTML = `<tr><th>Minutes</th><th>Load</th></tr>
            <tr><td>1</td><td class="min-1"></td></tr>
            <tr><td>5</td><td class="min-5"></td></tr>
            <tr><td>15</td><td class="min-15"></td></tr>`;
        this.progress = [new ProgressBar(), new ProgressBar(), new ProgressBar()];
        this.div.getElementsByClassName("min-1")[0].appendChild(this.progress[0].canvas);
        this.div.getElementsByClassName("min-5")[0].appendChild(this.progress[1].canvas);
        this.div.getElementsByClassName("min-15")[0].appendChild(this.progress[2].canvas);
    }
    update(completion) {
        // todo read /proc/loadavg
        // 1min 5min 15min
        $.get({
            url:"./php/proxy.php?url=/proc/loadavg",
            success: (response, statusText) => {
                let averages = response.split(" ");
                for(let i = 0; i < this.progress.length; ++i) {
                    this.progress[i].progress = averages[i];
                    this.progress[i].render();
                }
                if(completion)
                    completion();
            }
        });
    }
}
WidgetDb.addWidget("loadavg", (dataset) => {return new LoadAvgWidget(dataset);});

class TimeWidget {
    constructor() {
        this._div = document.createElement("div")
        this.updateInterval = 1000;
    }

    get div() {
        this.update();
        return this._div;
    }

    update(completion) {
        let date = new Date();
        var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        this._div.innerHTML = str;
        if(completion)
            completion();
    }
}


WidgetDb.addWidget("time", function(){return new TimeWidget();});

class IFrameWidget {
    constructor(dataset) {
        this.iframe = document.createElement(dataset.inject? "div" : "iframe");
        this.iframe.src = dataset.src;
        if(dataset.width)
            this.iframe.width = dataset.width;
        if(dataset.height)
            this.iframe.height = dataset.height;
    }

    get div() {
        return this.iframe;
    }

    update(completion) {
        if(this.inject) {
            $.get({
                url: this.iframe.src,
                success: (response) => {
                    this.iframe.innerHTML = response;
                    if(completion)
                        completion();
                },
                error: () => {
                    if(completion)
                        completion();
                }
            });
            return;
        }
        if(completion)
            completion();
    }
}
WidgetDb.addWidget("iframe", function(dataset){return new IFrameWidget(dataset);});