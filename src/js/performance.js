function timeFunction(func) {
    const start = performance.now();
    func();
    const total = (performance.now() - start)/1000.0;
    return total;
}