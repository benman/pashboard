function diffRatio(a, b) {
    return 2.0*Math.abs(a - b)/(a + b);
}


function potpack(boxes) {

    // calculate total box area and maximum box width
    let area = 0;
    let maxWidth = 0;

    for (const box of boxes) {
        area += box.w * box.h;
        maxWidth = Math.max(maxWidth, box.w);
    }

    // sort the boxes for insertion by height, descending
    boxes.sort((a, b) => b.h*b.w - a.w*a.h);

    // aim for a squarish resulting container,
    // slightly adjusted for sub-100% space utilization
    const startWidth = Math.max(Math.ceil(Math.sqrt(area)), maxWidth);
    const optimalSize = Math.ceil(Math.sqrt(area));

    // start with a single empty space, unbounded at the bottom
    const spaces = [{x: 0, y: 0, w: startWidth, h: Infinity}];

    let width = 0;
    let height = 0;

    let i = 0;
    console.log("initial spaces");
    console.log(JSON.stringify(spaces, false, 4));
    for (const box of boxes) {
        box.order = i;
        i += 1;
        console.log("iteration order " + box.order);
        
        // merge spaces vertically
        spaces.sort((a, b) => {
            let xd = a.x - b.x;
            if(xd != 0)
                return xd;
            return a.y - b.y;
        });
        for(let i = 0; i < spaces.length-1; ++i) {
            if(spaces[i].x != spaces[i+1].x)
                continue;
            if(spaces[i].w != spaces[i+1].w)
                continue;
            if((spaces[i].y + spaces[i].h) === spaces[i+1].y) {
                spaces[i].h += spaces[i+1].h;
                spaces.splice(i+1, 1);
            }
        }
        const minWidth = Math.max(optimalSize, width);
        const minHeight = Math.max(optimalSize, height);
        spaces.sort((a, b) => {
            if(a.h < box.h && b.h >= box.h)
                return 1;
            if(b.h < box.h && a.h >= box.h)
                return -1;
            let inBoundA = ((a.x + box.w) <= minWidth) && ((a.y + box.h) < minHeight)? 1 : 0;
            let inBoundB = ((b.x + box.w) <= minWidth) && ((b.y + box.h) < minHeight)? 1 : 0;
            let inBoundDiff = inBoundA - inBoundB;
            if(inBoundDiff != 0)
                return inBoundDiff;
            if(inBoundA === 0) {
                let expandA = {
                    width: Math.max(minWidth, (a.x + box.w)),
                    height: Math.max(minHeight, (a.y + box.h))
                };
                let expandB = {
                    width: Math.max(minWidth, (b.x + box.w)),
                    height: Math.max(minHeight, (b.y + box.h))
                };

                let squareDiff = diffRatio(expandA.width, expandA.height) - diffRatio(expandB.width, expandB.height);
                if(squareDiff != 0) {
                    return -squareDiff;
                }
            }
            let y = b.y - a.y;
            if(y != 0)
                return -y;
            let distanceDiff = a.x*a.x + a.y*a.y - (b.x*b.x + b.y*b.y);
            if(distanceDiff != 0)
                return distanceDiff;
            let area = b.w*b.h - a.w*a.h;
            if(area != 0)
                return area;
            let minDiff = Math.min(b.x, b.y) - Math.min(a.x, a.y);
            if(minDiff != 0)
                return -minDiff;
            
            let x = b.x - a.x;
            return -x;
        });
        console.log("after sorting");
        console.log(JSON.stringify(spaces, false, 4));
        console.log("for box");
        console.log(box);
        // look through spaces backwards so that we check smaller spaces first
        for (let i = spaces.length - 1; i >= 0; i--) {
            const space = spaces[i];
            if(box.h > space.h) continue;
            if((space.x + space.w) === startWidth && box.w > space.w) {
                let newWidth = space.x + box.w;
                console.log("expanding for order " + box.order);
                // expand it
                for(let spaceIt of spaces) {
                    if((spaceIt.x + spaceIt.w) == startWidth) {
                        spaceIt.w = newWidth - spaceIt.x;
                        if(spaceIt.w < 0) {
                            console.error("space.w < 0");
                            console.log({
                                space: spaceIt,
                                newWidth: newWidth
                            });
                        }
                    }
                }
            }
            if(space.w < box.w) {
                console.error("width is not big enough");
                console.log({space: space, box: box});
                continue;
            }
            console.log("picked space: ", space);
            console.log("i = ", i);
            // found the space; add the box to its top-left corner
            // |-------|-------|
            // |  box  |       |
            // |_______|       |
            // |         space |
            // |_______________|
            box.x = space.x;
            box.y = space.y;

            height = Math.max(height, box.y + box.h);
            width = Math.max(width, box.x + box.w);

            if (box.w === space.w && box.h === space.h) {
                // space matches the box exactly; remove it
                const last = spaces.pop();
                if (i < spaces.length) spaces[i] = last;

            } else if (box.h === space.h) {
                // space matches the box height; update it accordingly
                // |-------|---------------|
                // |  box  | updated space |
                // |_______|_______________|
                space.x += box.w;
                space.w -= box.w;
                if(space.w < 0) {
                    console.error("negative width");
                }
            } else if (box.w === space.w) {
                // space matches the box width; update it accordingly
                // |---------------|
                // |      box      |
                // |_______________|
                // | updated space |
                // |_______________|
                space.y += box.h;
                space.h -= box.h;

            } else {
                // otherwise the box splits the space into two spaces
                // |-------|-----------|
                // |  box  | new space |
                // |_______|___________|
                // | updated space     |
                // |___________________|
                spaces.push({
                    x: space.x + box.w,
                    y: space.y,
                    w: space.w - box.w,
                    h: box.h
                });
                space.y += box.h;
                space.h -= box.h;
            }
            
            break;
        }
    }

    let result = {w: width, h: height,
        fill: (area/(width*height)),
        lowerBound: optimalSize,
        startWidth: startWidth,
        boxes: boxes
    };
    console.log(result);
    return result;

}