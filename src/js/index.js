
class StringBuilder {
    constructor() {
        this.data = []
    }

    write() {
        let total = '';
        for(let i = 0; i < arguments.length; ++i) {
            total += arguments[i];
        }
        this.data.push(total)
    }

    writeAttributes(properties) {
        for(let property in properties) {
            if(!properties.hasOwnProperty(property))
                continue;

            if(property == "style" && typeof properties[property] == 'object') {
                let style = properties["style"];
                this.write(" style=\"");
                for(let name in style) {
                    this.write(name, ":", style[name], ";");
                }
                this.write("\"");
                continue;
            }
            this.write(" ", property, "=\"", properties[property], "\"");
        }
    }
    openTag(tag, properties) {
        this.write("<" + tag);
        if(!properties) {
            this.write(">");
            return;
        }
        this.writeAttributes(properties);

        this.write(">");
    }

    openDiv(className, properties) {
        if(!properties)
            properties = {};
        properties["class"] = className;
        this.openTag("div", properties)
    }
    closeDiv() {
        this.write("</div>");
    }
    emptyTag(tag, properties) {
        let end = "/>";
        if(tag == "iframe")
            end = "></iframe>";
        this.write("<" + tag);
        if(!properties) {
            this.write(end);
            return;
        }
        this.writeAttributes(properties);
        this.write(end);
    }

    closeTag(tag) {
        this.write("</" + tag + ">");
    }

    build() {
        return this.data.join('');
    }
}

function getArea(div) {
    return div.offsetWidth*div.offsetHeigth;
}

function getChildren(div) {
    let children = [];
    let first = div.firstChild;
    while(first) {
        if(first.nodeName != "#text")
            children.push(first);
        first = first.nextSibling;
    }
    return children;
}
class Dashboard {
    constructor(layout) {
        this.layoutStr = layout;
        this._div = document.createElement("div");
        this._div.innerHTML = layout;
        this._div.style.position = "absolute";
        this._div.allowRecurse = true;
        this.widgets = [];
        this.needsUpdate = true;
        for(let child of Array.from(this._div.getElementsByTagName("div"))) {
            child.allowRecurse = true;
            child.style.position = "absolute";
            if(child.dataset && child.dataset.widget) {
                this.install(child);
            }
        }
        globalUpdateQueue.interval(() => {this.layoutIfNeeded();}, 1000);
        this.needsLayout = true;
    }

    get div() {
        return this._div;
    }

    install(div) {
        if(!div.dataset) {
            console.error("no dataset");
        }
        if(div.widget)
            return;
        div.allowRecurse = false;
        console.log("installing " + div.dataset.widget);
        let widget = WidgetDb.newWidget(div.dataset.widget, div.dataset);
        this.widgets.push(widget);
        div.appendChild(widget.div);
        div.widget = widget;
        widget.update(() => {
            this.needsLayout = true;
        });
        if(widget.updateInterval) {
            console.log("new update: " + widget.updateInterval);
            globalUpdateQueue.interval(() => {
                const size = {
                    width: widget.div.offsetWidth,
                    height: widget.div.offsetHeight
                };
                widget.update(() => {
                    const newSize = {
                        width: widget.div.offsetWidth,
                        height: widget.div.offsetHeight
                    };
                    if(size.width !== newSize.width || size.height !== newSize.height)
                        this.needsLayout = true;
                });

            }, widget.updateInterval);
        }
    }
    layoutDiv(div) {
        let nodes = getChildren(div);
        if(nodes.length == 0)
            return;
        if(!div.allowRecurse) {
            return;
        }
        const boxes = [];
        for(let node of nodes) {
            this.layoutDiv(node);
            node.style.position = "absolute";
            node.style.left = "0px";
            node.style.top = "0px";
            if(node.dataset.width) {
                node.style.width = node.dataset.width + "px";
            }
            if(node.dataset.height)
                node.style.height = node.dataset.height + "px";
            let box = {x: 0, y: 0, w: node.offsetWidth, h: node.offsetHeight,
                node: node
            };
            
            boxes.push(box);
        }
        
        // we want largest first
        nodes.sort((a, b) => {
            return getArea(b) - getArea(a);
        });
        
        if(div.dataset.groupStyle == "square") {
            let total = potpack(boxes);
            for(let box of boxes) {
                box.node.style.left = box.x + "px";
                box.node.style.top = box.y + "px";
                box.node.dataset.order = box.order;
            }
            div.style.width = total.w + "px";
            div.style.height = total.h + "px";
        }
        
    }
    layout() {
        this.layoutDiv(this._div);
        this.needsLayout = false;
    }

    layoutIfNeeded() {
        if(this.needsLayout)
            this.layout();
    }


    update() {
        for(let widget of this.widgets) {
            widget.update();
        }
    }
}

var dashboard = null;
function loadDashboard(layout) {
    globalUpdateQueue.clear();
    dashboard = new Dashboard(layout);

    
    document.getElementById("dashboard").appendChild(dashboard.div);
    dashboard.layout();
}


window.onload = (() => {
    loadDashboard(`<div data-group-style="square">
    <div data-widget="iframe" data-src="./widgets/weather-toronto.html"
    data-width="400" data-height="210"></div> <div data-widget="iframe" data-src="./widgets/weather-toronto.html"
    data-width="400" data-height="210"></div> <div data-widget="iframe" data-src="./widgets/weather-toronto.html"
    data-width="400" data-height="210"></div> <div data-widget="iframe" data-src="file:///proc/cpuinfo"
    data-width="400" data-height="450"></div>
            <div data-widget="time"></div>
            <div data-widget="time"></div>
            <div data-widget="time"></div>
            <div data-widget="loadavg"></div>
            </div>`
    );

});