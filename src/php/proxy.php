<?php
if($_SERVER["REMOTE_ADDR"] !== "127.0.0.1") {
    exit(1);
}

$url = $_GET["url"];

if($url[0] == '/') {
    $data = file_get_contents($url);
    echo($data);
    exit(0);
}